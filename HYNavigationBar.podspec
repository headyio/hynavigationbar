Pod::Spec.new do |s|  
    s.name                    = 'HYNavigationBar'
    s.version                 = '1.0.0'
    s.summary                 = 'HYNavigationBar is a custom navigation bar which replicates iOS UINavigationBar.'
    s.homepage                = 'https://www.heady.io'

    s.author                  = { 'HYNavigationBar' => 'tirupati@heady.io' }
    s.license                 = { :type => 'MIT', :file => 'LICENSE' }

    s.platform                = :ios
    s.source                  = { :git => 'https://bitbucket.org/headyio/hynavigationbar.git', :branch => "codebase" }
    s.source_files            = 'Sources/*.{swift}'
    s.resources               = 'HYNavigationBar.bundle'
    s.ios.deployment_target   = '11.0'
    s.swift_versions          = '5.0'
end 
