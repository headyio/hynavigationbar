//
//  HYNavigationBar.swift
//  WCM
//
//  Created by Tirupati Balan on 08/01/21.
//  Copyright © 2021 Heady. All rights reserved.
//

import Foundation
import UIKit

public class HYNavigationBar: UIView {

    /// Get constraints status of UI element
    public var didSetupConstraints = false
    
    /// Get navigation type
    public var navigationType: NavigationType = .largeWithProgressAndBack
    
    /// Init with navigation type
    /// - Parameters:
    ///   - type : Navigation wrapper from previous coordinator
    public init(_ type: NavigationType) {
        self.navigationType = type
        
        var frame = CGRect.zero
        switch self.navigationType {
        case .largeWithoutProgress:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.largeNavigationHeight))
        case .largeWithProgress:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.largeNavigationWithProgressBarHeight))
            backButton.isHidden = true
        case .largeWithoutProgressAndBack:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.largeNavigationHeight))
            backButton.isHidden = true
        case .largeWithProgressAndBack:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.largeNavigationWithProgressBarHeight))
        case .normalWithProgress:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.normalNavigationHeightWithProgress))
        case .normalWithProgressAndBack:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.normalNavigationHeightWithProgress))
        case .normalWithoutProgress:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.normalNavigationHeight))
        case .normalWithoutProgressAndBack:
            frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: HYNavigationBar.Constants.normalNavigationHeight))
        }
        super.init(frame: frame)
        setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: Private Variables
    public struct Constants {
        public static let normalNavigationHeight: CGFloat = 44.0
        public static let normalNavigationHeightWithProgress: CGFloat = 56.0
        public static let largeNavigationHeight: CGFloat = 96.0
        public static let largeNavigationWithProgressBarHeight: CGFloat = 108.0

        static let progressBarHeight: CGFloat = 12.0
        static let navigationPromptHeight: CGFloat = 52.0
        static let backButtonLeadingInset: CGFloat = 22.0
        static let backButtonTopInset: CGFloat = 12.0
        static let titleLeadingTrailingInset: CGFloat = 22.0
        static let titleFontSize: CGFloat = 34.0
        static let navigationLeadingTrailing: CGFloat = 22.0
        static let bottomLineHeight: CGFloat = 1.0
        static let progressBackgroundColor: UIColor = UIColor(red: 87.0 / 255.0,
                                                              green: 131.0 / 255.0,
                                                              blue: 221.0 / 255.0,
                                                              alpha: 1.0)
        static let progressTintColor: UIColor = UIColor(red: 55.0 / 255.0,
                                                        green: 199.0 / 255.0,
                                                        blue: 139.0 / 255.0,
                                                        alpha: 1.0)
        static let navigationBackgroundColor: UIColor = UIColor(red: 246.0 / 255.0,
                                                                green: 248.0 / 255.0,
                                                                blue: 250.0 / 250.0,
                                                                alpha: 1)
        static let backImage: String = "HYNavigationBar.bundle/hyBack.png"
    }
    
    private var normalNavigationBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private var navigationPromptView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private var topProgressBarView: UIProgressView = {
        let view = UIProgressView()
        view.backgroundColor = Constants.progressBackgroundColor
        view.progressTintColor = Constants.progressTintColor
        return view
    }()
    
    // MARK: Public Variables
    public var backButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 0)
        button.tintColor = .black
        return button
    }()

    public var rightButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        return button
    }()
    
    public var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        return label
    }()

    public var bottomLineLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .lightGray
        return label
    }()

    func getBundle() -> Bundle? {
        let frameworkBundle = Bundle(for: HYNavigationBar.self)
        return frameworkBundle
    }
    
    // MARK: Setup views
    func setupUI() {
        backgroundColor = Constants.navigationBackgroundColor
        addSubview(topProgressBarView)
        addSubview(normalNavigationBarView)
        addSubview(navigationPromptView)
        
        if navigationType == .normalWithoutProgress ||
            navigationType == .normalWithProgress ||
            navigationType ==  .normalWithProgressAndBack ||
            navigationType ==  .normalWithoutProgressAndBack {
            normalNavigationBarView.addSubview(titleLabel)
            titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
            titleLabel.textAlignment = .center
        } else {
            navigationPromptView.addSubview(titleLabel)
            titleLabel.font = UIFont.boldSystemFont(ofSize: 34)
            titleLabel.textAlignment = .left
        }
        navigationPromptView.addSubview(rightButton)
        normalNavigationBarView.addSubview(backButton)
        addSubview(bottomLineLabel)
        
        let backImage = UIImage(named: Constants.backImage,
                                  in: getBundle(),
                                  compatibleWith: nil)
        backButton.setImage(backImage?.withRenderingMode(.alwaysTemplate),
                        for: .normal)

        setBottomLineHidden(true)
    }
    
    // MARK: - Constraints -
    public override func updateConstraints() {
        if !didSetupConstraints {
            topProgressBarView.translatesAutoresizingMaskIntoConstraints = false
            if navigationType == .largeWithoutProgressAndBack ||
                navigationType == .largeWithoutProgress ||
                navigationType == .normalWithoutProgress ||
                navigationType == .normalWithoutProgressAndBack {
                NSLayoutConstraint.activate([
                    topProgressBarView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                    topProgressBarView.leadingAnchor.constraint(equalTo: leadingAnchor),
                    topProgressBarView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                    topProgressBarView.heightAnchor.constraint(equalToConstant: 0)
                ])
            } else {
                NSLayoutConstraint.activate([
                    topProgressBarView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                    topProgressBarView.leadingAnchor.constraint(equalTo: leadingAnchor),
                    topProgressBarView.trailingAnchor.constraint(equalTo: trailingAnchor),
                    topProgressBarView.heightAnchor.constraint(equalToConstant: HYNavigationBar.Constants.progressBarHeight)
                ])
            }
            
            normalNavigationBarView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                normalNavigationBarView.topAnchor.constraint(equalTo: topProgressBarView.bottomAnchor),
                normalNavigationBarView.leadingAnchor.constraint(equalTo: leadingAnchor),
                normalNavigationBarView.trailingAnchor.constraint(equalTo: trailingAnchor),
                normalNavigationBarView.heightAnchor.constraint(equalToConstant: HYNavigationBar.Constants.normalNavigationHeight)
            ])
            
            navigationPromptView.translatesAutoresizingMaskIntoConstraints = false
            if navigationType == .normalWithoutProgress ||
                navigationType == .normalWithProgress ||
                navigationType ==  .normalWithProgressAndBack ||
                navigationType ==  .normalWithoutProgressAndBack {
                NSLayoutConstraint.activate([
                    navigationPromptView.topAnchor.constraint(equalTo: normalNavigationBarView.bottomAnchor),
                    navigationPromptView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                    navigationPromptView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                    navigationPromptView.bottomAnchor.constraint(equalTo: bottomAnchor),
                    navigationPromptView.heightAnchor.constraint(equalToConstant: 0)
                ])
                
                titleLabel.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    titleLabel.widthAnchor.constraint(equalTo: normalNavigationBarView.widthAnchor, multiplier: 0.45),
                    titleLabel.bottomAnchor.constraint(equalTo: normalNavigationBarView.bottomAnchor),
                    titleLabel.topAnchor.constraint(equalTo: normalNavigationBarView.topAnchor),
                    titleLabel.centerXAnchor.constraint(equalTo: normalNavigationBarView.centerXAnchor)
                ])

            } else {
                NSLayoutConstraint.activate([
                    navigationPromptView.topAnchor.constraint(equalTo: normalNavigationBarView.bottomAnchor),
                    navigationPromptView.leadingAnchor.constraint(equalTo: leadingAnchor),
                    navigationPromptView.trailingAnchor.constraint(equalTo: trailingAnchor),
                    navigationPromptView.bottomAnchor.constraint(equalTo: bottomAnchor)
                ])
                
                titleLabel.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    titleLabel.leadingAnchor.constraint(equalTo: navigationPromptView.leadingAnchor, constant: HYNavigationBar.Constants.titleLeadingTrailingInset),
                    titleLabel.trailingAnchor.constraint(equalTo: navigationPromptView.trailingAnchor, constant: HYNavigationBar.Constants.titleLeadingTrailingInset),
                    titleLabel.bottomAnchor.constraint(equalTo: navigationPromptView.bottomAnchor),
                    titleLabel.topAnchor.constraint(equalTo: navigationPromptView.topAnchor)
                ])
            }

            rightButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                rightButton.widthAnchor.constraint(equalToConstant: HYNavigationBar.Constants.normalNavigationHeight),
                rightButton.trailingAnchor.constraint(equalTo: navigationPromptView.trailingAnchor),
                rightButton.bottomAnchor.constraint(equalTo: navigationPromptView.bottomAnchor),
                rightButton.topAnchor.constraint(equalTo: navigationPromptView.topAnchor)
            ])

            backButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                backButton.widthAnchor.constraint(equalToConstant: HYNavigationBar.Constants.normalNavigationHeight),
                backButton.leadingAnchor.constraint(equalTo: leadingAnchor),
                backButton.topAnchor.constraint(equalTo: normalNavigationBarView.topAnchor),
                backButton.bottomAnchor.constraint(equalTo: normalNavigationBarView.bottomAnchor)
            ])
            
            bottomLineLabel.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                bottomLineLabel.heightAnchor.constraint(equalToConstant: HYNavigationBar.Constants.bottomLineHeight),
                bottomLineLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
                bottomLineLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
                bottomLineLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
            didSetupConstraints = true
        }
        super.updateConstraints()
    }
    
    /// Set navigation type
    /// - Parameters:
    ///   - navigationType : NavigationType enum type
    public func setType(_ navigationType: NavigationType) {
        self.navigationType = navigationType
    }

    /// Set back button color based on image
    /// - Parameters:
    ///   - color : BackButtonColor enum type
    public func setBackButtonColor(_ color: BackButtonColor) {
        switch color {
        case .black:
            backButton.tintColor = .black
        case .white:
            backButton.tintColor = .white
        }
    }
    
    /// Set show/hide status of bottom line
    /// - Parameters:
    ///   - value : Bool value
    public func setBottomLineHidden(_ value: Bool) {
        bottomLineLabel.isHidden = value
    }
    
    /// Set background color for top progress view
    /// - Parameters:
    ///   - value : Background color
    public func setProgressBackgroundColor(_ value: UIColor) {
        topProgressBarView.backgroundColor = value
    }
    
    /// Set progress tint color for top progress view
    /// - Parameters:
    ///   - value : Tint color
    public func setProgressTintColor(_ value: UIColor) {
        topProgressBarView.progressTintColor = value
    }
    
    /// Set progress for top progress view
    /// - Parameters:
    ///   - value : Progress value
    public func setProgress(value: Float) {
        DispatchQueue.main.async {
            self.topProgressBarView.setProgress(value, animated: true)
        }
    }
}
