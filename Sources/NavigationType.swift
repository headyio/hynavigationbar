//
//  NavigationType.swift
//  HYNavigationBarDemo
//
//  Created by Tirupati Balan on 02/03/21.
//

import Foundation

/// To get different type of navigation bar based on type
public enum NavigationType: Int {
    /// To get normalWithProgress type of navigation bar
    case normalWithProgress
    
    /// To get normalWithProgressAndBack type of navigation bar
    case normalWithProgressAndBack
    
    /// To get normalWithoutProgress type of navigation bar
    case normalWithoutProgress
    
    /// To get normalWithoutProgressAndBack type of navigation bar
    case normalWithoutProgressAndBack
    
    /// To get largeWithProgress type of navigation bar
    case largeWithProgress
    
    /// To get largeWithProgressAndBack type of navigation bar
    case largeWithProgressAndBack
    
    /// To get largeWithoutProgress type of navigation bar
    case largeWithoutProgress
    
    /// To get largeWithoutProgressAndBack type of navigation bar
    case largeWithoutProgressAndBack
}
