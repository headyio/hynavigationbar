//
//  BackButtonColor.swift
//  HYNavigationBarDemo
//
//  Created by Tirupati Balan on 02/03/21.
//

import Foundation

/// To get back button color
public enum BackButtonColor {
    /// Get back button with tint color of type white
    case white
    
    /// Get back button with tint color of type black
    case black
}
